// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:flutter/material.dart';

import 'main.dart';
import 'package:katlehomotloenyamodule4/user_profile.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

List<Widget> pages = [
  const Dashboard(),
  const Screen1(),
  const Screen2(),
  const UserProfile(),
];

class NotOut {
  final Widget child;
  NotOut({required this.child});

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final GlobalKey<ScaffoldState> _key = GlobalKey();
    SizedBox(height: size.height * 0.03);
    return Scaffold(
      backgroundColor: Colors.orange[100],
      key: _key,
      body: Stack(
        children: [
          Align(
            alignment: const Alignment(0.91, -0.91),
            child: FloatingActionButton(
              heroTag: "logout",
              onPressed: () => {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => const LoginPage()),
                )
              },
              tooltip: 'Logout',
              child: const Icon(Icons.logout),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: child,
          )
        ],
      ),
    );
  }
}

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: NotOut(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Screen1()),
                  );
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 50),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  textStyle: const TextStyle(color: Colors.white),
                ),
                child: const Text(
                  "Screen 1",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const Screen1()),
                  );
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.orange,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 50),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  textStyle: const TextStyle(color: Colors.white),
                ),
                child: const Text(
                  "Screen 2",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ).build(context),
    );
  }
}

class MyDashboard extends StatefulWidget {
  const MyDashboard({Key? key}) : super(key: key);

  @override
  State<MyDashboard> createState() => _MyDashboardState();
}

class _MyDashboardState extends State<MyDashboard> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages.elementAt(index),
      bottomNavigationBar: CurvedNavigationBar(
        backgroundColor: Colors.orange,
        items: const [
          Icon(Icons.dashboard),
          Icon(Icons.abc),
          Icon(Icons.gif_box),
          Icon(Icons.person),
        ],
        color: Colors.orangeAccent,
        buttonBackgroundColor: Colors.blue,
        animationCurve: Curves.easeInCubic,
        animationDuration: const Duration(milliseconds: 600),
        onTap: (_index) {
          setState(() {
            index = _index;
          });
        },
      ),
    );
  }
}

class Screen1 extends StatelessWidget {
  const Screen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: NotOut(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[Text('Screen 1')],
          ),
        ),
      ).build(context),
    );
  }
}

class Screen2 extends StatelessWidget {
  const Screen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: NotOut(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const <Widget>[Text('Screen 2')],
          ),
        ),
      ).build(context),
    );
  }
}
